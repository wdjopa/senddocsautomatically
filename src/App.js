import "./App.css";
import "antd/dist/antd.css";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import ListBills from "./bills/list/ListBills";
import Layout from "./components/Layout";

function App() {
  return (
    <Layout>
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<ListBills />} />
        </Routes>
      </BrowserRouter>
    </Layout>
  );
}

export default App;
