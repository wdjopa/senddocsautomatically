import React from "react";
import { Card } from "antd";
const { Meta } = Card;
function CardBill() {
  return (
    <Card hoverable style={{ width: 240 }} cover={<img alt="example" src="https://data.unhcr.org/images/documents/big_aa2c81585e808b644ef70587136c23601d33a2e9.jpg" />}>
      <Meta title="Facture du loyer d'Anonty" description="Dernier envoie le 21/08/2022"/>
    </Card>
  );
}

export default CardBill;
