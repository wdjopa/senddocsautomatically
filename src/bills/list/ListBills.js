import React from 'react'
import CardBill from './CardBill'
import NewBill from './NewBill';

function ListBills() {
  return (
    <div class="py-3 flex">
      <CardBill />
      <NewBill />
    </div>
  );
}

export default ListBills