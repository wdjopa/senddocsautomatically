import React from "react";
import { Card } from "antd";
const { Meta } = Card;
function NewBill() {
  // center the text in the card
  return (
    <Card hoverable style={{ width: 240 }} className="align-middle ">
      <div>+ Ajouter une facture</div>
    </Card>
  );
}

export default NewBill;
