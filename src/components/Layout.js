import React from "react";

function Layout({ children }) {
  return (
    <div>
      <nav className="bg-gray-800">
        <div className="max-w-7xl mx-auto px-2 sm:px-6 lg:px-8">
          <div className="relative flex items-center justify-between h-16 text-white">Send Automate Bills</div>
        </div>
      </nav>
      <main className="max-w-7xl mx-auto px-2 sm:px-6 lg:px-8">{children}</main>
    </div>
  );
}

export default Layout;
